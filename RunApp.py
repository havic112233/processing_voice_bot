import asyncio
from aiogram.dispatcher import FSMContext

from loader import bot
from Data import creater_id


async def on_startup(dp):
    await bot.send_message(creater_id, "Бот запущен!")

async def on_shutdown(dp):
    await bot.close()

if __name__ == "__main__":
    from aiogram import executor
    from handlers import dp

    executor.start_polling(dp, on_startup=on_startup, on_shutdown=on_shutdown)