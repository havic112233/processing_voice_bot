import asyncio

import requests
import aiogram.types
import urllib.request
import json

from Data import IAM_TOKEN,FOLDER_ID, TOKEN
from aiogram.types import Message, CallbackQuery
from aiogram.dispatcher.filters import Command, Text
from loader import bot as BOT
from loader import dp
from keyboards import Authenticated, startButton


# /start
@dp.message_handler(Command("start"))
async def hello(message: Message):
    await message.answer(f"Привет {message.from_user.first_name}! Что хотите сделать?", reply_markup=Authenticated)

# /help
@dp.message_handler(Command("help"))
async def help(message: Message):
    await message.answer(text=("The bot supports voice messages, try the next commands:\n- Привет, Кира,\n- Подскажи курс доллара по ЦБ\n- Какой баланс-счет\n- Какие рекомендации по ставкам\n\n<b>And else some text to help the user.</b>"), parse_mode='html')


@dp.callback_query_handler(text='close')
async def unknown_message(call: CallbackQuery):
    await call.message.edit_reply_markup(reply_markup=None)
    await call.message.answer(text="До встречи!", reply_markup=startButton)


@dp.message_handler(content_types=['voice'])
async def voice_processing(message: Message):
    # from handlers.voice_recognition import recognition #перенести в начало файла
    # await recognition(message)
    from handlers.voice_recognition import recognition_voise #перенести в начало файла
    await recognition_voise(message)


@dp.message_handler()
async def unknown_message(message: Message):
    """Ответ на любое неожидаемое сообщение"""
    unknown_text = "Ничего не понятно, но очень интересно.\nПопробуй команду /help"
    await message.answer(unknown_text)