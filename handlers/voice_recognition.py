import urllib
import json
from datetime import datetime
from xml.etree import ElementTree

import requests
from loader import bot as BOT
from aiogram.types import Message
from Data import IAM_TOKEN, TOKEN, FOLDER_ID


# async def recognition(message: Message):
#     file_info = await BOT.get_file(message.voice.file_id)
#     file = requests.get(f'https://api.telegram.org/file/bot{TOKEN}/{file_info.file_path}')
#
#     with urllib.request.urlopen(file.url) as f:
#         data = f.read()
#
#     params = "&".join([
#         "topic=general",
#         "folderId=%s" % FOLDER_ID,
#         "lang=ru-RU"
#     ])
#     url = urllib.request.Request("https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?%s" % params, data=data)
#     url.add_header("Authorization", "Api-Key %s" % IAM_TOKEN)
#     responseData = urllib.request.urlopen(url).read().decode('UTF-8')
#     decodedData = json.loads(responseData)
#
#     if decodedData.get("error_code") is None:
#         print(decodedData.get("result"))
#         user_voice_message = decodedData.get("result")
#
#         if "Привет Кира".lower() == user_voice_message.lower() or "Привет" == user_voice_message:
#             await message.answer(text=f"Здравствуй {message.from_user.first_name}. Чем могу помочь?")
#
#         elif "Подскажи курс доллара по ЦБ".lower() == user_voice_message.lower() or "курс доллара" in user_voice_message.lower():
#
#             url = f"http://www.cbr.ru/scripts/XML_daily.asp?date_req={datetime.now().strftime('%d/%m/%Y')}"
#             zip_country = 'R01235'
#
#             with urllib.request.urlopen(url, timeout=10) as cbr:
#                 usd = (ElementTree.parse(cbr).findtext(f".//Valute[@ID='{zip_country}']/Value"))
#                 number = usd.replace(',', '.')
#                 current_price_usd = float(number)
#             await message.answer(text=f"Курс доллара по ЦБ сейчас: {current_price_usd}")
#
#         elif "Какой баланс-счет" == user_voice_message or "баланс" in user_voice_message.lower():
#             await message.answer(text=f"Ваш баланс на текущий момент составляет: {None} USD")
#
#         elif "Какие рекомендации по ставкам" == user_voice_message \
#                 or "Рекомендации по ставкам" == user_voice_message \
#                 or "Какие рекомендации поставкам" == user_voice_message \
#                 or "Рекомендации поставкам" == user_voice_message:
#             await message.answer(text=f"Рекомендаций временно отсутствуют.")
#
#         else:
#             await message.answer(text=f"Неизвестная команда. Доступны пока четыре. Воспользуйтесь /help чтобы их узнать.")
#
#     else:
#         print("error_code is True")


class ErrorCodeServer(Exception):
    def __init__(self, error_text="Ошибка при получении voice message"):
        self.error = error_text

async def get_USD_exchange_rate():
    url = f"http://www.cbr.ru/scripts/XML_daily.asp?date_req={datetime.now().strftime('%d/%m/%Y')}"
    zip_country = 'R01235'

    with urllib.request.urlopen(url, timeout=10) as cbr:
        usd = (ElementTree.parse(cbr).findtext(f".//Valute[@ID='{zip_country}']/Value"))
        number = usd.replace(',', '.')
        current_price_usd = float(number)
    return current_price_usd


async def recognition_voise(message):

    file_info = await BOT.get_file(message.voice.file_id)
    file = requests.get(f'https://api.telegram.org/file/bot{TOKEN}/{file_info.file_path}')

    with urllib.request.urlopen(file.url) as f:
        data = f.read()

    params = "&".join([
        "topic=general",
        "folderId=%s" % FOLDER_ID,
        "lang=ru-RU"
    ])

    url = urllib.request.Request("https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?%s" % params, data=data)
    url.add_header("Authorization", "Api-Key %s" % IAM_TOKEN)

    responseData = urllib.request.urlopen(url).read().decode('UTF-8')
    decodedData = json.loads(responseData)

    if decodedData.get("error_code") is None:
        user_voice_message = decodedData.get("result")
        await text_answer(user_voice_message, message)

    else:
        raise ErrorCodeServer()

async def text_answer(user_voice_message, message):

        if "Привет Кира".lower() == user_voice_message.lower() \
                or "Привет" == user_voice_message:
            await message.answer(text=f"Здравствуй {message.from_user.first_name}. Чем могу помочь?")

        elif "Подскажи курс доллара по ЦБ".lower() == user_voice_message.lower() \
                or "курс доллара" in user_voice_message.lower():
            current_price_USD = await get_USD_exchange_rate()
            await message.answer(text=f"Курс доллара по ЦБ сейчас: {current_price_USD}")

        elif "Какой баланс-счет" == user_voice_message or "баланс" in user_voice_message.lower():
            await message.answer(text=f"Ваш баланс на текущий момент составляет: {None} BTC")

        elif "Какие рекомендации по ставкам" == user_voice_message \
                or "Рекомендации по ставкам" == user_voice_message \
                or "Какие рекомендации поставкам" == user_voice_message \
                or "Рекомендации поставкам" == user_voice_message:
            await message.answer(text=f"Рекомендаций временно отсутствуют.")

        else:
            await message.answer(text=f"Неизвестная команда. Доступны пока четыре. Воспользуйтесь"
                                      f" /help чтобы их узнать.")