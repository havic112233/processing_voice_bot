from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton


Authenticated = InlineKeyboardMarkup(
    inline_keyboard=[
        [
            InlineKeyboardButton(text="Перейти на сайт!", url='https://www.bybit.com/login')
        ],
        [
            InlineKeyboardButton(text="Войти в личный кабинет", callback_data='enter'),
            InlineKeyboardButton(text="Закрыть", callback_data='close')
        ]
    ]
)