from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

startButton = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='/start'),
        ]
    ],
    resize_keyboard=True
)